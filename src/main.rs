mod weather;
mod server;


#[tokio::main]
async fn main() {
    let e = northstar::Server::bind(("localhost", 1965))
        .serve(server::parkbench_serve).await;
    if let Err(e) = e {
        for e in e.chain() {
            println!("{:?}", e);
        }
    }
}
