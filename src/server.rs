use northstar::{
    GEMINI_MIME,
    Request,
    Response,
    types::{
        Meta,
        ResponseHeader,
        Status,
    }
};

use futures::future::{
    BoxFuture,
    FutureExt,
};

use anyhow::Result;

use crate::weather::WeatherConfig;

pub fn parkbench_serve(request: Request) -> BoxFuture<'static, Result<Response>> {
    let path = request.path_segments();
    let response = if path[0].is_empty() {
        homepage_serve()
    } else {
        match path[0].as_str() {
            "board" => board_serve(&path[1..]),
            _ => Response::not_found(),
        }
    };
    async move {
        Ok(response)
    }.boxed()
}

pub fn homepage_serve() -> Response {
    let weather = WeatherConfig::new("Outer space").current_weather();

    let resp = format!(
        include_str!("../pages/index.gmi"),
        weather = weather,
    );

    Response::success(&GEMINI_MIME)
        .with_body(resp)
}

pub fn board_serve(path: &[String]) -> Response {
    if path.is_empty() {
        Response::new(ResponseHeader {
            status: Status::REDIRECT_PERMANENT,
            meta: Meta::new_lossy("board/")
        })
    } else {
        let resp = match path[0].as_str() {
            "" => include_str!("../pages/board/index.gmi"),
            "accessable.gmi" => include_str!("../pages/board/accessable.gmi"),
            _ => include_str!("../pages/board/back.gmi")
        };

        Response::success(&GEMINI_MIME)
            .with_body(resp)
    }
}
